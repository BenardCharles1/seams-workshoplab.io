---
fullname: Mohau Jacob Mateyisi
goby: teyisman
img: Mohau.jpg
links:
  -
    title: LinkedIn
    url: https://www.linkedin.com/in/mateyisi
affiliation:
  -
    org: Council for Scientific and Industrial Research -- South Africa
    position: Post-doc CSIR
---
