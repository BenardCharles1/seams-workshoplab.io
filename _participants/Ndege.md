---

fullname: Benard Charles Ndege
goby: Benard
img: benard.JPG
links:
  -
    title: Msc Student
    url: https://www.linkedin.com/in/bcn2018/
  -
    title: Data Scientist 
    url: https://www.ensibuuko.com/
affiliation:
  -
    org: Ensibuuko Tech. Limited
    position: Data Scientist
  -
    org: AIMS - Senegal
    position: Msc Student
---
